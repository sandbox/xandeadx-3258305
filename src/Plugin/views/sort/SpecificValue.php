<?php

namespace Drupal\views_sort_by_specific_value\Plugin\views\sort;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * @ViewsSort("specific_value")
 */
class SpecificValue extends SortPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    $options['value'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $this->options['value'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function sortOptions(): array {
    return [
      'DESC' => $this->t('Records with specific value will be at beginning'),
      'ASC' => $this->t('Records with specific value will be at end'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function query(): void {
    $this->ensureMyTable();
    $query = $this->query; /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $sort_alias = $this->tableAlias . '_' . $this->field;

    $escaped_value = Database::getConnection()->quote($this->options['value']);
    $query->addOrderBy(NULL, "($this->tableAlias.$this->realField = $escaped_value)", $this->options['order'], $sort_alias);
  }

}
