<?php

namespace Drupal\views_sort_by_specific_value\Plugin\views\sort;

/**
 * @ViewsSort("specific_value_in_multivalue_field")
 */
class SpecificValueInMultiValueField extends SpecificValue {

  /**
   * {@inheritDoc}
   */
  public function query(): void {
    /** @see \Drupal\views\Plugin\views\HandlerBase::ensureMyTable() */

    $query = $this->query; /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $sort_name = $this->field;
    $base_table = $this->view->storage->get('base_table');
    $field_table = $this->table;
    $field_value_column_name = $this->realField;
    $join = $query->getJoinData($field_table, $base_table);
    $escaped_value = $query->getConnection()->quote($this->options['value']);

    $sort_expression = "
      ($escaped_value IN (
        SELECT f.$field_value_column_name
        FROM $field_table f
        WHERE f.$join->field = $base_table.$join->leftField
      ))
    ";
    $query->addOrderBy(NULL, $sort_expression, $this->options['order'], $base_table . '_' . $sort_name);
  }

}
